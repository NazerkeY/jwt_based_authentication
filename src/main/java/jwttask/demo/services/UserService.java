package jwttask.demo.services;

import jwttask.demo.models.Response;
import jwttask.demo.models.User;
import jwttask.demo.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService implements UserDetailsService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userRepository.findByEmail(email);
        if(user != null){
            return user;
        }
        else{
            responseEntity();
            throw new UsernameNotFoundException(String.format("User with %s email cannot be found", email));

        }
    }

    public ResponseEntity<?> responseEntity(){
        return new ResponseEntity<>(new Response("Check your email"), HttpStatus.CONFLICT);
    }

    public boolean checkUser(String email){
        User userFromData = userRepository.findByEmail(email);
        if(userFromData != null){
            return true;
        }
        else{
            return false;
        }
    }
}
