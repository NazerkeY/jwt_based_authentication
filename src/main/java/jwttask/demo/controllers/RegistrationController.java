package jwttask.demo.controllers;

import jwttask.demo.models.Response;
import jwttask.demo.models.Role;
import jwttask.demo.models.User;
import jwttask.demo.repositories.UserRepository;
import jwttask.demo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.Collections;

@RestController
public class RegistrationController {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @Autowired
    private UserService userService;

    @GetMapping("/signup")
    public String signup(Model model){
        return "signup";
    }

    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    public ResponseEntity<?> signUp(@Valid @RequestBody User user) throws Exception {
        if(!userService.checkUser(user.getEmail())) {
            if (user.getPassword().equals(user.getConfirmPassword())) {
                user.setRoles(Collections.singleton(new Role("ROLE_USER")));
                user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
                userRepository.save(user);
                return new ResponseEntity<>(new Response(user.toString()), HttpStatus.CREATED);
            }
            else{
                return new ResponseEntity<>(new Response("Passwords don't match!"), HttpStatus.CONFLICT);
            }
        }
        else{
            return new ResponseEntity<>(new Response("User already exist!"), HttpStatus.ALREADY_REPORTED);
        }
    }
}
